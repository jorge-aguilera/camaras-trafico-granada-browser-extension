

$(document).ready(function() {
    function createTd( camaras ){
        let ret = `<ul class="list-group col-lg-3">`;        
        for( c in camaras){            
            ret+= `<li class="list-group-item">
            <input class="form-check-input" type="checkbox" id="#Camara${camaras[c].num}" value="${camaras[c].num}">
            <button type="button" class="btn btn-info" 
                    data-toggle="tooltip" data-html="true"
                    title="<img width='640' height='480' src='${camaras[c].url}'>" >
                    <span class="btn-label"><i class="glyphicon glyphicon-camera"></i></span>Preview
            </button>
            <label class="form-check-label" for="#Camara${camaras[c].num}">${camaras[c].name}</label> 
            </li>`
        }
        ret+='</ul>'
        return ret;
    }

    $('#salvar').on('click',function(){
        alert("ummm todavia no guardamos las preferencias, pero estamos en ello oye")
    });

    prepareCameras(function( camaras){              
        let tmp = [];
        for( c in camaras){            
            if( camaras[c].num < 0 )continue;
            tmp.push(camaras[c])
        }        
        for(var c=0; c<tmp.length; c++){
            let slice = tmp.slice(c, c+10);
            c+=20;            
            let html = createTd(slice);            
            $(html).appendTo('#personalizadas');            
        }
        $('[data-toggle="tooltip"]').tooltip()
    });
  
  });